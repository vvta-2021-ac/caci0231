package AngajatiApp.controller;

import AngajatiApp.model.Employee;
import AngajatiApp.repository.EmployeeMock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static AngajatiApp.controller.DidacticFunction.TEACHER;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeControllerTest {

    private Employee e1;
    EmployeeController employeeController;
    int nrEmployee;

    @BeforeEach
    void setUp() {
        e1=new Employee();
        e1.setFirstName("Popescu");
        e1.setLastName("Mihai");
        try{e1.setCnp("1781212053256");}
        catch(Exception e)
        {}
        e1.setFunction(TEACHER);
        e1.setSalary(5000d);
        EmployeeMock employeeMock = new EmployeeMock();
        EmployeeController employeeController = new EmployeeController(employeeMock);
        try {
            nrEmployee = employeeController.getEmployeesList().size();
        }
        catch (Exception e)
        {}
        System.out.println("setUp");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    void addEmployeeT1() {
        try{
            employeeController.addEmployee(e1);
            assert(false);//nu s-a aruncat exceptie
            assertEquals(nrEmployee+1,employeeController.getEmployeesList().size());
        }
        catch (Exception e)
        {
            assert(true);//nu s-a prins exceptie
        }
    }
    /* testu 2 nu il pot testa ca nu accepta alt string in afara de cele din didactic function
    @Test
    void adaugaEmployeeT2() {
        try{
            String s1 = "profesor";
            e1.setFunction(s1);
            employeeController.addEmployee(e1);
            assert  (false); // s-a aruncat exceptie, nu exista acea functie didactica
             }
        catch(Exception e)
        {
            assert(true);//s-a prins exceptie
            try{assertEquals(nrEmployee,employeeController.getEmployeesList().size());}
            catch(Exception e2){}
        }
    }*/
//testu 3 ECP
    @Test
    void adaugaEmployeeT3() {
        try{

            e1.setSalary(-200d);
            employeeController.addEmployee(e1);
            assert  (false); // s-a aruncat exceptie, nu poate sa fie salarul un numar negativ
        }
        catch(Exception e)
        {
            assert(true);//s-a prins exceptie
            try{assertEquals(nrEmployee,employeeController.getEmployeesList().size());}
            catch(Exception e2){}
        }
    }

    @Test
    void adaugaEmployeeT4() {
        try{

            e1.setCnp("asdwad2512");
            employeeController.addEmployee(e1);
            assert  (false); // s-a aruncat exceptie, CNP este un numar de 13 cifre
        }
        catch(Exception e)
        {
            assert(true);//s-a prins exceptie
            try{assertEquals(nrEmployee,employeeController.getEmployeesList().size());}
            catch(Exception e2){}
        }
    }

    @Test
    void adaugaEmployeeT5() {
        try{

            e1.setSalary(null);
            employeeController.addEmployee(e1);
            assert  (false); // s-a aruncat exceptie, salariul nu poate fi null
        }
        catch(Exception e)
        {
            assert(true);//s-a prins exceptie
            try{assertEquals(nrEmployee,employeeController.getEmployeesList().size());}
            catch(Exception e2){}
        }
    }

    @Test
    void adaugaEmployeeT6() {
        try{

            e1.setCnp("999999999999");
            employeeController.addEmployee(e1);
            assert  (false); // s-a aruncat exceptie, CNP trebuie sa aiba 13 cifre.. nu 12
        }
        catch(Exception e)
        {
            assert(true);//s-a prins exceptie
            try{assertEquals(nrEmployee,employeeController.getEmployeesList().size());}
            catch(Exception e2){}
        }
    }

    @Test
    void adaugaEmployeeT7() {
        try{

            e1.setFirstName("Po");
            employeeController.addEmployee(e1);
            assert  (false); // s-a aruncat exceptie, FirstName este de minim 3 cifre
        }
        catch(Exception e)
        {
            assert(true);//s-a prins exceptie
            try{assertEquals(nrEmployee,employeeController.getEmployeesList().size());}
            catch(Exception e2){}
        }
    }

    @Test
    void adaugaEmployeeT15() {
        try{

            e1.setCnp("100000000000000");
            employeeController.addEmployee(e1);
            assert  (false); // s-a aruncat exceptie, CNP trebuie sa aiba exact 13 cifre.. nu 12
        }
        catch(Exception e)
        {
            assert(true);//s-a prins exceptie
            try{assertEquals(nrEmployee,employeeController.getEmployeesList().size());}
            catch(Exception e2){}
        }
    }

    @Test
    void adaugaEmployeeT8() {
        try{

            e1.setFirstName("Pop");
            employeeController.addEmployee(e1);
            assert  (false); // s-a aruncat exceptie, FirstName este de minim 3 cifre
        }
        catch(Exception e)
        {
            assert(true);//s-a prins exceptie
            try{assertEquals(nrEmployee,employeeController.getEmployeesList().size());}
            catch(Exception e2){}
        }
    }

}