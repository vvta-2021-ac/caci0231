package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
   Employee Marin   = new Employee("Larson", "Puscas", "1234567890870", DidacticFunction.TEACHER,  2500d);

    @Test
    void modifyEmployeeFunction() {
        EmployeeMock repoEmployee = new EmployeeMock();
        repoEmployee.modifyEmployeeFunction(Marin, DidacticFunction.TEACHER);
        assertEquals(DidacticFunction.TEACHER,Marin.getFunction());
    }

    @Test
    void modifyEmployeeFunctionTest() {
        EmployeeMock repoEmployee = new EmployeeMock();
        Employee e1 = null;
        repoEmployee.modifyEmployeeFunction(e1,DidacticFunction.CONFERENTIAR);
        boolean gasit = true;
        int i= 0;
        while(i<repoEmployee.getEmployeeList().size())
        {
            Employee em = repoEmployee.getEmployeeList().get(i);
            if(em.getFunction()==DidacticFunction.CONFERENTIAR)
                gasit = false;
            i++;
        }
        assertEquals(true,gasit);
        /*try{
            repoEmployee.modifyEmployeeFunction(e1,DidacticFunction.TEACHER);
            assert  (true); // s-a aruncat exceptie, nu se poate modifica functia didactica unui angajat null
        }
        catch(Exception e)
        {
            assert(false);//s-a prins exceptie
        }*/
    }
}